# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
#
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files
#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
#
#
# Danishka Navin <danishka@gmail.com>, 2009, 2011.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdebconf-terminal@packages.debian.org\n"
"POT-Creation-Date: 2019-11-02 21:09+0100\n"
"PO-Revision-Date: 2011-09-15 07:01+0530\n"
"Last-Translator: \n"
"Language-Team: Sinhala <info@hanthana.org>\n"
"Language: si\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: text
#. Description
#. :sl3:
#: ../cdebconf-gtk-terminal.templates:1001
msgid "Resume installation"
msgstr "ස්ථාපනය නැවත අරඹමින්"

#. Type: text
#. Description
#. :sl3:
#: ../cdebconf-gtk-terminal.templates:2001
msgid ""
"Choose \"Continue\" to really exit the shell and resume the installation; "
"any processes still running in the shell will be aborted."
msgstr ""
"ඇත්ත වශයෙන්ම ශෙල් එකෙන් ඉවත් වී ස්ථාපනය නැවත ආරම්භ කිරීමට \"දිගටම කරගෙන යන්න\" තෝරන්න; "
"දැනටමත් ශෙල් එකේ ධාවනය වන ඕනෑම ක්‍රියාවක් නැවැත්වෙනු ඇත."
